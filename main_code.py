import ast
import sys, os
import desing
import xlrd, xlwt
import platform

from xlutils.copy import copy


from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog, QErrorMessage, QMessageBox


class ExampleApp(QMainWindow, desing.Ui_MainWindow):
    def __init__(self, parent=None):
        super(ExampleApp, self).__init__(parent)
        self.setupUi(self)
        self.pushButton.clicked.connect(self.browse_file_delimiter)
        self.pushButton_2.clicked.connect(self.delimiter)
        self.pushButton_3.clicked.connect(self.browse_file_from_site_combiner)
        self.pushButton_4.clicked.connect(self.browse_dir_files_combiner)
        self.pushButton_5.clicked.connect(self.combiner)
        self.winFileName = None
        self.winFileSiteName = None
        self.winDirName = None

    def browse_file_delimiter(self):
        options = QFileDialog.Options()
        # options |= QFileDialog.toNative
        file_name, _ = QFileDialog.getOpenFileName(self, "Укажите файл реестра", "",
                                                  "Excel Files (*.xls);;All Files (*)", options=options)
        """  "All Files (*);;Excel Files (*.xls)"   """
        if file_name:
            self.winFileName = os.path.abspath(file_name)
            self.textBrowser.setFontPointSize(12)
            self.textBrowser.setText(self.winFileName)

    def browse_file_from_site_combiner(self):
        file_name, _ = QFileDialog.getOpenFileName(self, "Укажите файл реестра загруженный с сайта", "",
                                                  "Excel Files (*.xls);;All Files (*)")
        if file_name:
            self.winFileSiteName = os.path.abspath(file_name)
            self.textBrowser_2.setFontPointSize(12)
            self.textBrowser_2.setText(self.winFileSiteName)

    def browse_dir_files_combiner(self):
        dir_name = QFileDialog.getExistingDirectory(self, "Укажите папку с файлами показаний счетчиков")

        if dir_name:
            self.winDirName = os.path.abspath(dir_name)
            self.textBrowser_3.setFontPointSize(12)
            self.textBrowser_3.setText(self.winDirName)

        """Индексы столбцов в файле
        N(0), Номер(1), Лицевой счет(2), Дом(3), Помещение (4), Счетчик(5), Код(6), Вид показаний(7)
        Начальные показания(8), Конечные показания(9), Количество(10), Дата начала(11),Дата окончания(12)
        Дата последний(13)"""

    def delimiter(self):
        if self.winFileName:
            if platform.system() == 'Linux':
                outdir = os.path.dirname(self.winFileName)+'/out/'
            else:
                outdir = os.path.dirname(self.winFileName) + '\\out\\'
            """открываем исходный файл"""
            workbook = xlrd.open_workbook(self.winFileName, on_demand=True, formatting_info=True)
            worksheet = workbook.sheet_by_index(0)
            """инициализация в памяти первого файла"""
            book = xlwt.Workbook(encoding='utf-8')
            sheet1 = book.add_sheet('Лист1')
            """Адрес для названия файла"""
            address = worksheet.cell(1, 3).value
            """стили для ячеек"""
            bold = xlwt.easyxf("font: bold on; align: horiz center")
            bold_and_grey = xlwt.easyxf("font: bold on; pattern: pattern solid, fore_colour gray25; align: horiz center")
            center = xlwt.easyxf("align: horiz center")
            """заполнение названий столбцов"""
            column_names = [worksheet.cell_value(0, col, ) for col in range(worksheet.ncols)]
            for index, value in enumerate(column_names):
                sheet1.write(0, index, value, bold_and_grey)
                """столбцам которые хотим скрыть ставим 0 ширину"""
                if index in [0, 1, 2, 3, 6, 11, 12]:
                    sheet1.col(index).width = 0
                    continue
                elif index == 7:
                    sheet1.col(index).width = 4500
                    continue
                elif index == 13:
                    sheet1.col(index).width = 7300
                    continue
                elif index == 8:
                    sheet1.col(index).width = 6500
                    continue
                sheet1.col(index).width = 5924 #ширина яйчейки 5924
            progress = 0 # начальное значение прогрессбара
            new_doc_row_counter = 1 # номер строки при занесении в каждый новый файл
            for i in range(1, worksheet.nrows):
                """если строки идут от другого адреса сохраняем файл и создаем новый"""
                if address != worksheet.cell(i, 3).value:
                    """если каталог для файлов не существует - создаем, иначе нет"""
                    if not os.path.exists(outdir):
                        os.mkdir(outdir)
                    """сохраняем старый и создаем новый файл с новыйми загаловками столбцов"""
                    if platform.system() == 'Linux':
                        book.save(outdir + '/' + address + '.xls')
                    else:
                        book.save(outdir + '\\' + address + '.xls')
                    book = xlwt.Workbook(encoding='utf-8')
                    sheet1 = book.add_sheet('Лист1')
                    address = worksheet.cell(i, 3).value
                    for index, value in enumerate(column_names):
                        sheet1.write(0, index, value, bold_and_grey)
                        if index in [0, 1, 2, 3, 6, 11, 12]:
                            sheet1.col(index).width = 0
                            continue
                        elif index == 7:
                            sheet1.col(index).width = 4500
                            continue
                        elif index == 13:
                            sheet1.col(index).width = 7300
                            continue
                        sheet1.col(index).width = 5924
                    """обновляем счетчик строк для новго файла"""
                    new_doc_row_counter = 1
                    progress += 1.81 #получается где то 55 файлов 100/55 для заполнения прогрессбара
                    """заполняем погрессюар"""
                    self.progressBar.setValue(progress)
                """если строка пренадлежит одному адресу то продолжаем их заполнять"""
                data = [worksheet.cell_value(i, col) for col in range(worksheet.ncols)]
                for index, value in enumerate(data):
                    """форматирования столбца >Помещение<"""
                    if index == 4:
                        sheet1.write(new_doc_row_counter, index, value, bold)
                        #вид показаний
                    elif index == 7:
                        sheet1.write(new_doc_row_counter, index, value, center)
                        # проверяем что начальные показания не записаны как строка
                    elif index == 8 and isinstance(value, str):
                        #если да, то заменяем запятую на точку и преобразуем в число
                        sheet1.write(new_doc_row_counter, index, float(value.replace(',', '.')),center)
                    #конечные значения делаем пустыми
                    elif index == 9:
                        sheet1.write(new_doc_row_counter, index, None, center)
                    #записываем формулу 'Jномер строки - Iномер строки' в ячейку <Количество>
                    elif index == 10:
                        row = str(new_doc_row_counter+1)
                        formula = 'J'+row+'-'+'I'+row
                        sheet1.write(new_doc_row_counter, index, xlwt.Formula(formula), center)
                    #Дата
                    elif index == 13:
                        sheet1.write(new_doc_row_counter, index, value, center)
                    else:
                    #остальное переносим без изменений
                        sheet1.write(new_doc_row_counter, index, value)
                """после заполнения строки увелииваем счетчик строк в файле"""
                new_doc_row_counter += 1
            """если проценты недобежали до 100 после обработки всех файлов и программа отработала,
            то добиваем процент до 100"""
            book.save(outdir + '\\' + address + '.xls')#сохраняем последний файл с адресом
            if progress < 100:
                self.progressBar.setValue(100)
        #если файл не выбран, то сообщаем об этом
        else:
            self.textBrowser.setFontPointSize(14)
            self.textBrowser.setText('Выберите файл!')

    def combiner(self):
        if self.winFileSiteName and not self.winDirName:
            self.textBrowser_3.setFontPointSize(14)
            self.textBrowser_3.setText('Выберите папку с файлами!')
        elif self.winDirName and not self.winFileSiteName:
            self.textBrowser_2.setFontPointSize(14)
            self.textBrowser_2.setText('Выберите файл!')
        elif not self.winDirName and not self.winFileSiteName:
            self.textBrowser_2.setFontPointSize(14)
            self.textBrowser_3.setFontPointSize(14)
            self.textBrowser_2.setText('Выберите файл!')
            self.textBrowser_3.setText('Выберите папку с файлами!')
        else:
            if platform.system() == 'Linux':
                prefix = '/'
                outdir = os.path.dirname(self.winFileSiteName)+'/out/'
            else:
                prefix = '\\'
                outdir = os.path.dirname(self.winFileSiteName) + '\\out\\'
            """открываем исходный файл"""
            workbook = xlrd.open_workbook(self.winFileSiteName, on_demand=True, formatting_info=True)
            worksheet = workbook.sheet_by_index(0)
            address = ''
            not_exist_file_name = ''
            file = None
            progress = 0
            bold = xlwt.easyxf("font: bold on; align: horiz center")
            last_counter_id = None
            last_counter_num = None
            suka = None
            for i in range(1, worksheet.nrows):                
                if address != worksheet.cell(i, 3).value:
                    """если каталог для файлов не существует - создаем, иначе нет"""
                    if not os.path.exists(outdir):
                        os.mkdir(outdir)
                    """сохраняем старый и создаем новый файл с новыйми загаловками столбцов"""
                    file = outdir+address + '.xls'
                    if not os.path.isfile(file) and not_exist_file_name != address:
                        wb.save(outdir + prefix + address + '.xls')
                    address = worksheet.cell(i, 3).value

                    """Внимание  try except FileNotFound сообщение и пауза программы гуглить !!!!"""
                    try:
                        rb = xlrd.open_workbook(self.winDirName+prefix+address+'.xls', formatting_info=True)

                    except IOError:
                        not_exist_file_name = address
                        print('Не найден файл %s.xls' % address)
                        print(self.winDirName+prefix+address+'.xls')
                        continue

                    r_sheet = rb.sheet_by_index(0)
                    wb = copy(rb)  # a writable copy (I can't read values out of this, only write to it)
                    w_sheet = wb.get_sheet(0)  # the sheet to write to within the writable copy
                    progress += 1.81  # получается где то 55 файлов 100/55 для заполнения прогрессбара
                    """заполняем погрессбар"""
                    self.progressBar_2.setValue(progress)
                """перебераю отдельный адрес, если знаение"""

                if not_exist_file_name and not_exist_file_name != address:
                    continue

                for row_num in range(1, r_sheet.nrows):
                    row_values = r_sheet.row_values(row_num)
                    formula = 'J' + str(row_num + 1) + '-' + 'I' + str(row_num + 1)
                    w_sheet.write(row_num, 10, xlwt.Formula(formula))
                    """Ищем в файле с адресом счетчик по полю Код"""
                    if worksheet.cell(i,6).value == row_values[6]:
                        """если нашли сверяем значение"""
                        if last_counter_id == row_values[6] and last_counter_num == row_values[8]:
                            continue
                        if row_values[9] == '':
                            cell8_value = float(worksheet.cell(i, 8).value.replace(',', '.'))
                            """
                            try:
                                expr = int(worksheet.cell(i, 9).value - int(cell8_value))

                            except TypeError:
                                print(int(worksheet.cell(i, 9).value), type(int(worksheet.cell(i, 9).value)), type(worksheet.cell(i, 9).value))
                                print(int(cell8_value), type(int(cell8_value)), cell8_value)
                                expr = None"""
                            if 'Электроэнергия' == worksheet.cell(i, 5).value and (int(worksheet.cell(i, 9).value - int(cell8_value))) > 1000:

                                w_sheet.write(row_num, 9, worksheet.cell(i, 9).value, bold)
                                last_counter_id = row_values[6]
                                last_counter_num = row_values[8]
                                break

                            elif 'Х' in worksheet.cell(i, 5).value and \
                                    (worksheet.cell(i, 9).value - cell8_value > 30):
                                w_sheet.write(row_num, 9, worksheet.cell(i, 9).value, bold)
                                last_counter_id = row_values[6]
                                last_counter_num = row_values[8]
                                break
                            elif 'Г' in worksheet.cell(i, 5).value and \
                                    (worksheet.cell(i, 9).value - cell8_value > 20):
                                #print('blya',row_num, worksheet.cell(i, 9).value,bold)
                                w_sheet.write(row_num, 9, worksheet.cell(i, 9).value, bold)
                                last_counter_id = row_values[6]
                                last_counter_num = row_values[8]
                                break
                            else:
                                w_sheet.write(row_num, 9, worksheet.cell(i, 9).value)
                                last_counter_id = row_values[6]
                                last_counter_num = row_values[8]
                                break
                        elif worksheet.cell(i, 9).value > row_values[9]:
                            w_sheet.write(row_num, 9, worksheet.cell(i, 9).value, bold)
                            last_counter_id = row_values[6]
                            last_counter_num = row_values[8]
                            break
                        #print(address, row_num, worksheet.cell(i,6), row_values[9], worksheet.cell(i,9).value)
                        #w_sheet.write(row_num, 9, worksheet.cell(i,9))

                if i == (worksheet.nrows - 1):
                    wb.save(outdir + address + '.xls')  # сохраняем последний файл с адресом
                    if progress != 100:
                        self.progressBar_2.setValue(100)


def main():
    app = QApplication(sys.argv)
    form = ExampleApp()
    form.show()
    app.exec_()


if __name__ == '__main__':
    main()
